import UIKit

class Solution {
    func myAtoi(_ s: String) -> Int {
        var value = ""
        var foundMinus = false
        var foundPlus = false
        var startNumeric = false
        for char in s {
            if char == " " {
                if startNumeric { break }
                continue
            } else if char == "+" {
                if foundMinus || foundPlus || startNumeric { break }
                foundPlus = true
            } else if char == "-" {
                if foundMinus || foundPlus || startNumeric { break }
                foundMinus = true
            } else if let numeric = Int16(char.description) {
                startNumeric = true
                value += numeric.description
            } else {
                break
            }
        }
        if value == "" {
            return 0
        }
        if let numeric = Int32(value) {
            return Int(foundMinus ? numeric * -1 : numeric)
        } else {
            return Int(foundMinus ? Int32.min : Int32.max)
        }
    }
}


print(Solution().myAtoi("words and 987"))
