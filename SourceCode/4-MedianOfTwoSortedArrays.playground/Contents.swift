import UIKit

class Solution {
    
    func getValueAtIndex(_ index: Int, _ nums1: [Int], _ nums2: [Int]) -> (Int, Int) {
        var firstIndex = 0
        var secondIndex = 0
        var currentValue: (Int, Int) = (0, 0)
        for _ in 0...index {
            if nums1[firstIndex] < nums2[secondIndex] {
                currentValue = (currentValue.1, nums1[firstIndex])
                firstIndex += 1
            } else {
                currentValue = (currentValue.1, nums2[secondIndex])
                secondIndex += 1
            }
        }
        return currentValue
    }
    
    func findMedianSortedArrays(_ nums1: [Int], _ nums2: [Int]) -> Double {
        let totalCount = nums1.count + nums2.count
        if totalCount == 1 {
            return Double(nums1.count == 0 ? nums2[0] : nums1[0])
        }
        let indexUpTo = totalCount / 2
        var indexes: (Int, Int) = (nums1.count > 0 ? 0 : -1, nums2.count > 0 ? 0 : -1)
        var currentValue: (Int, Int) = (0, 0)
        for _ in 0...indexUpTo {
            if indexes.0 >= 0 && indexes.1 >= 0 {
                if nums1[indexes.0] < nums2[indexes.1] {
                    currentValue = (currentValue.1, nums1[indexes.0])
                    indexes.0 = (nums1.count > (indexes.0 + 1)) ? indexes.0 + 1 : -1
                } else {
                    currentValue = (currentValue.1, nums2[indexes.1])
                    indexes.1 = (nums2.count > (indexes.1 + 1)) ? indexes.1 + 1 : -1
                }
            } else if indexes.0 < 0 {
                currentValue = (currentValue.1, nums2[indexes.1])
                indexes.1 += 1
            } else {
                currentValue = (currentValue.1, nums1[indexes.0])
                indexes.0 += 1
            }
        }
        return totalCount % 2 == 0 ? Double(currentValue.0 + currentValue.1) / 2.0 : Double(currentValue.1)
    }
}

let num1: [Int] = [3,4]
let num2: [Int] = []

let solution = Solution()
print(solution.findMedianSortedArrays(num1, num2))
