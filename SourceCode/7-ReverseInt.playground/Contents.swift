import UIKit

class Solution {
    func reverse(_ x: Int) -> Int {
        var x = x
        var reversed = 0
        while x != 0 {
            reversed = (reversed * 10) + (x % 10)
            x = x / 10
        }
        if reversed >= Int32.min && reversed <= Int32.max {
            return reversed
        }
        return 0
    }
}
print(Solution().reverse(1534236469))
print(Solution().reverse(1240))
