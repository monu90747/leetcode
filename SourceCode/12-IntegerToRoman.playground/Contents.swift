import UIKit

class Solution {
    
    func getNextRomansRange(_ max: String) -> (min: String, mid: String, max: String) {
        if max == "" {
            return (min: "I", mid: "V", max: "X")
        } else if max == "X" {
            return (min: "X", mid: "L", max: "C")
        } else if max == "C" {
            return (min: "C", mid: "D", max: "M")
        } else {
            return (min: "M", mid: "", max: "")
        }
    }
    
    func getRomanValue(_ num: Int, r: (min: String, mid: String, max: String)) -> String {
        switch num {
        case 1: return r.min
        case 2: return r.min + r.min
        case 3: return r.min + r.min + r.min
        case 4: return r.min + r.mid
        case 5: return r.mid
        case 6: return r.mid + r.min
        case 7: return r.mid + r.min + r.min
        case 8: return r.mid + r.min + r.min + r.min
        case 9: return r.min + r.max
        default: return ""
        }
    }
    
    func intToRoman(_ num: Int) -> String {
        var maxRange = ""
        var result = ""
        var num = num
        while num > 0 {
            let romanRange = getNextRomansRange(maxRange)
            result = getRomanValue(num % 10, r: romanRange) + result
            maxRange = romanRange.max
            num /= 10
        }
        return result
    }
}

print(Solution().intToRoman(1999))
