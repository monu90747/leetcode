import UIKit

class Solution {
    func longestCommonPrefix(_ strs: [String]) -> String {
        var result = ""
        for charIndex in 0..<strs[0].count {
            let matchingChar = strs[0][strs[0].index(strs[0].startIndex, offsetBy: charIndex)]
            for value in strs {
                if value.count > charIndex {
                    if matchingChar != value[value.index(value.startIndex, offsetBy: charIndex)] {
                        return result
                    }
                } else {
                    return result
                }
            }
            result += matchingChar.description
        }
        return result
    }
}

print("Result:", Solution().longestCommonPrefix(["flower","flow","flight"]))
