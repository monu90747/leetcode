import UIKit

class Solution {
    
    func romanToInt(_ s: String) -> Int {
        var s = s
        var result = 0
        while s.count > 0 {
            let value = s.removeLast()
            if value == "I" {
                result += 1
            } else if value == "V" {
                if s.last == "I" {
                    result += 4
                    s.removeLast()
                } else {
                    result += 5
                }
            } else if value == "X" {
                if s.last == "I" {
                    result += 9
                    s.removeLast()
                } else {
                    result += 10
                }
            } else if value == "L" {
                if s.last == "X" {
                    result += 40
                    s.removeLast()
                } else {
                    result += 50
                }
            } else if value == "C" {
                if s.last == "X" {
                    result += 90
                    s.removeLast()
                } else {
                    result += 100
                }
            } else if value == "D" {
                if s.last == "C" {
                    result += 400
                    s.removeLast()
                } else {
                    result += 500
                }
            } else if value == "M" {
                if s.last == "C" {
                    result += 900
                    s.removeLast()
                } else {
                    result += 1000
                }
            }
        }
        return result
    }
}


print(Solution().romanToInt("MCMXCIV"))
