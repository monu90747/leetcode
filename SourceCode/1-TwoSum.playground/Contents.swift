import UIKit

class Solution {
    func twoSum(_ nums: [Int], _ target: Int) -> [Int] {
        for firstIndex in 0..<nums.count {
            for secondIndex in (firstIndex + 1)..<nums.count {
                if nums[firstIndex] + nums[secondIndex] == target {
                    return [firstIndex, secondIndex]
                }
            }
        }
        return []
    }
}
