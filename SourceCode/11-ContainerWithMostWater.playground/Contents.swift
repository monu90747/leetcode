import UIKit

class Solution {
    func maxArea(_ height: [Int]) -> Int {
        var maxArea = 0
        var columnA = 0
        var columnB = height.count - 1
        
        while columnA < columnB {
            maxArea = max(maxArea, (columnB - columnA) * min(height[columnA], height[columnB]))
            height[columnA] < height[columnB] ? (columnA += 1) : (columnB -= 1)
            
//            if height[columnA] < height[columnB] {
//                let newArea = height[columnA] * (columnB - columnA)
//                if newArea > maxArea {
//                    maxArea = newArea
//                }
//                columnA += 1
//            } else {
//                let newArea = height[columnB] * (columnB - columnA)
//                if newArea > maxArea {
//                    maxArea = newArea
//                }
//                columnB -= 1
//            }
        }
        return maxArea
    }
}

print(Solution().maxArea([1,8,6,2,5,4,8,3,7]))
