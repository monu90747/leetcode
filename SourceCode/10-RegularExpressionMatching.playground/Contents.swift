import UIKit

class Solution {
    func isMatch(_ s: String, _ p: String) -> Bool {
        if p == ".*" { return true }
        var string = s
        var pattern = Array(p)
        var isSkipMatch = false
        while pattern.count != 0 {
            let value = pattern.removeFirst()
            if pattern.first == "*" {
                pattern.removeFirst()
                if value == "." {
                    isSkipMatch = true
                } else {
                    while true {
                        if value == string.first {
                            string.removeFirst()
                            isSkipMatch = false
                        } else if isSkipMatch == true {
                            string.removeFirst()
                        } else {
                            break
                        }
                    }
                }
            } else if value == "." {
                string.removeFirst()
            } else if value == string.first {
                string.removeFirst()
            } else {
                return false
            }
        }
        return string.count == 0
    }
}

//print(Solution().isMatch("mississippi", "mis*is*p*."))
print(Solution().isMatch("aaa", "a*a"))
