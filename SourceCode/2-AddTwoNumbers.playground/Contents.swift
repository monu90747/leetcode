import UIKit

/**
 You are given two non-empty linked lists representing two non-negative integers. The digits are stored in reverse order, and each of their nodes contains a single digit. Add the two numbers and return the sum as a linked list.

 You may assume the two numbers do not contain any leading zero, except the number 0 itself.
 
 Input: l1 = [2,4,3], l2 = [5,6,4]
 Output: [7,0,8]
 Explanation: 342 + 465 = 807.
 Example 2:

 Input: l1 = [0], l2 = [0]
 Output: [0]
 Example 3:

 Input: l1 = [9,9,9,9,9,9,9], l2 = [9,9,9,9]
 Output: [8,9,9,9,0,0,0,1]
 
 
 Constraints:
 The number of nodes in each linked list is in the range [1, 100].
 0 <= Node.val <= 9
 It is guaranteed that the list represents a number that does not have leading zeros.
 
 */

public class ListNode {
    public var val: Int
    public var next: ListNode?
    public init() { self.val = 0; self.next = nil; }
    public init(_ val: Int) { self.val = val; self.next = nil; }
    public init(_ val: Int, _ next: ListNode?) { self.val = val; self.next = next; }
}

class Solution {
    
    func addListNode(_ l1: ListNode?, _ l2: ListNode?, _ reminder: Int, _ original: ListNode?) {
        let total = (l1?.val ?? 0) + (l2?.val ?? 0) + reminder
        if l1?.next == nil && l2?.next == nil {
            if total > 9 {
                original?.val = total % 10
                original?.next = ListNode(1)
            } else {
                original?.val = total
            }
        } else if total > 9 {
            original?.next = ListNode(0)
            addListNode(l1?.next, l2?.next, 1, original?.next)
        } else {
            original?.next = ListNode(0)
            addListNode(l1?.next, l2?.next, 0, original?.next)
        }
        
        if total > 9 {
            original?.val = total % 10
        } else {
            original?.val = total
        }

        if l1?.next == nil && l2?.next == nil {
            if total > 9 {
                original?.next = ListNode(1)
            }
            return
        } else {
            original?.next = ListNode(0)
            addListNode(l1?.next, l2?.next, total > 9 ? 1 : 0, original?.next)
        }
    }
    
    func addTwoNumbers(_ l1: ListNode?, _ l2: ListNode?) -> ListNode? {
        let finalNode = ListNode(0)
        addListNode(l1, l2, 0, finalNode)
        return finalNode
    }
}

let l1 = ListNode(4)
let l2 = ListNode(6)

let solution = Solution()
let result = solution.addTwoNumbers(l1, l2)
print(result?.val, result?.next?.val)

