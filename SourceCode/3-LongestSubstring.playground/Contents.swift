import UIKit

func lengthOfLongestSubstring(_ s: String) -> Int {
    var result = 0
    var longestString = ""
    for char in s {
        if longestString.contains(char) {
            var newString = longestString
            for newChar in longestString {
                if newChar == char {
                    if result < longestString.count {
                        result = longestString.count
                    }
                    newString.removeFirst()
                    longestString = newString + char.description
                    break
                } else {
                    newString.removeFirst()
                }
            }
        } else {
            longestString += char.description
        }
    }
    return longestString.count > result ? longestString.count : result
}

let result = lengthOfLongestSubstring("abb")
print(result)
