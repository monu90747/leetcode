import UIKit

/**
 Given a string s, return the longest palindromic substring in s.

  

 Example 1:

 Input: s = "babad"
 Output: "bab"
 Note: "aba" is also a valid answer.
 Example 2:

 Input: s = "cbbd"
 Output: "bb"
 Example 3:

 Input: s = "a"
 Output: "a"
 Example 4:

 Input: s = "ac"
 Output: "a"
  

 Constraints:

 1 <= s.length <= 1000
 s consist of only digits and English letters (lower-case and/or upper-case),
 */

class Solution {
    
    func findMaxRange(_ characters: [Character], left: Int, right: Int) -> (start: Int, length: Int) {
        var left = left, right = right
        while left >= 0 && right < characters.count && characters[left] == characters[right] {
            left -= 1
            right += 1
        }
        return (left + 1, (right - left) - 1)
    }
    
    func longestPalindrome(_ s: String) -> String {
        if s.count <= 1 {
            return s
        }
        let characters = Array(s)
        var longestRange: (start: Int, length: Int) = (0, 0)
        for index in 0..<characters.count {
            let evenPelindrom = findMaxRange(characters, left: index, right: index + 1)
            let oddPelindrom = findMaxRange(characters, left: index, right: index + 2)
            
            let range = evenPelindrom.length < oddPelindrom.length ? oddPelindrom : evenPelindrom
            if longestRange.length < range.length {
                longestRange = range
            }
        }
        return String(characters[longestRange.start...longestRange.length - 1 + longestRange.start])
    }
}

print(Solution().longestPalindrome("cbabsd"))

