import UIKit

class Solution {
    func isPalindrome(_ x: Int) -> Bool {
        if x < 0 { return false }
        if x <= 9 { return true }
        var value = x
        var pelindrom = 0
        while value != 0 {
            pelindrom = (pelindrom * 10) + (value % 10)
            value /= 10
        }
        return pelindrom == x
    }
}

print(Solution().isPalindrome(-121))
