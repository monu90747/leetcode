import UIKit

class Solution {
    func threeSum(_ nums: [Int]) -> [[Int]] {
        guard nums.count >= 3 else { return [] }
        let nums = nums.sorted()
        var result = [[Int]]()
        for i in 0..<nums.count - 2 {
            if i > 0 && nums[i] == nums[i-1] { continue }
            var j = i + 1
            var k = nums.count - 1

            while j < k {
                if nums[j] + nums[k] == -nums[i] {
                    result.append([nums[i], nums[j], nums[k]])
                    
                    repeat {
                        j += 1
                    } while nums[j] == nums[j-1] && j < k
                    
                    repeat {
                        k -= 1
                    } while nums[k] == nums[k+1] && j < k
                } else if nums[j] + nums[k] < -nums[i] {
                    j += 1
                } else {
                    k -= 1
                }
            }
        }
        return result
    }
}

print("Result:", Solution().threeSum([-1,0,1,2,-1,-4]))
